const fs = require("fs")
const path = require("path")

function problem2(){
    fs.readFile("./lipsum.txt", 'utf-8', (err, data) =>{
        if(err){
            console.log(err);
        } else {
            console.log("Accessed to file");
        } 
        let upper_case = data.toUpperCase()
        fs.writeFile("./upperCase.txt", upper_case,(err) =>{
            if(err){
                console.log(err);
            } else {
                console.log("File written");
            } 
            fs.appendFile("./filesnames.txt", "upperCase.txt",(err)=>{
                if(err){
                    console.log(err);
                } else {
                    console.log("File appened");
                } 
                fs.readFile("./lipsum.txt", 'utf-8', (err,data) =>{
                    if(err){
                        console.log(err);
                    } else {
                        console.log("Accessed to file");
                    } 
                    let lower_case = data.toLowerCase().split(".").join(".\n")
                    fs.writeFile("./lowerCase.txt", lower_case,(err)=>{
                        if(err){
                            console.log(err);
                        } else {
                            console.log("File written");
                        }
                        fs.appendFile("./filesnames.txt", "$lowerCase.txt",(err) =>{
                            if(err){
                                console.log(err);
                            } else {
                                console.log("File Appended");
                            }
                            fs.readFile("./upperCase.txt", 'utf-8', (err,data) =>{
                                if(err){
                                    console.log(err);
                                } else {
                                    console.log("Accessed to file");
                                }
                                const file1 = data
                                fs.readFile("./lowerCase.txt", 'utf-8', (err,data) =>{
                                    if(err){
                                        console.log(err);
                                    } else {
                                        console.log("Accessed to file");
                                    }
                                    const file2 = data
                                    const concat = file1 + file2
                                    const sorted = (concat.split("\n").join("").split(" ").join("").split(".").join("").split("").sort().join(""))
                                    fs.writeFile("./sorted.txt", sorted, (err) =>{
                                        if(err){
                                            console.log(err);
                                        } else {
                                            console.log("File sorted");
                                        }
                                        fs.appendFile("./filesnames.txt", "$sorted.txt", (err) =>{
                                            if(err){
                                                console.log(err);
                                            } else {
                                                console.log("File appended");
                                            }
                                            fs.readFile("./filesnames.txt", 'utf-8', (err,data) =>{
                                                if(err){
                                                    console.log(err);
                                                } else {
                                                    console.log("Accessed to file");
                                                }
                                                let arr = data.split("$")
                                                for(let i =0; i<arr.length; i++){
                                                    fs.unlink(`./${arr[i]}`,(err)=>{
                                                        if(err){
                                                            console.log(err);
                                                        } else {
                                                            console.log("Files Deleted");
                                                        }
                                                    })
                                                }
                                            })
                                        }) 
                                    })
                                })
                            })
                        })
                    })
                })
            })
        })
    })
}

module.exports=problem2;

