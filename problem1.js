const fs = require("fs");
const path = require("path");

function problem1(){
    fs.mkdir(path.join(__dirname, "intro"), (err) =>{
        if(err){
            console.log(err);
        } else {
            console.log("Directory Created");
        } fs.writeFile("./intro/name.json", '{"Name":"Hemanth"}', (err) =>{
            if(err){
                console.log(err);
            } else {
                console.log("File Written");
            } fs.unlink("./intro/name.json", (err) =>{
                if(err){
                    console.log(err);
                } else {
                    console.log("File Deleted");
                }
            })
        })
    })
}

module.exports = problem1;